<?php

namespace Jdesca\Infra\Service;

use Jdesca\Infra\Entity\Article;

class CapitalizeService
{

    public function capitalize(Article $article){
        $article->setTitle(ucwords($article->getTitle()));
    }
}