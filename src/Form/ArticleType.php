<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 25/08/20
 * Time: 15:53
 */

namespace Jdesca\Infra\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder,$options);
        $builder->add('title', TextType::class);
    }

    public function setDefaultOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Jdesca\Infra\Article',
            'empty_data' => null,
            'allow_extra_fields' => true,
        ));
    }

}